
/**
 *
 * @author Troy Alcaraz
 */
public class GuiMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        GuiWindow theWindow = new GuiWindow();
        theWindow.setVisible(true);
    }
    
}
