import java.io.File;
import java.util.Scanner;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;

import java.util.stream.Collectors;

/**
 * This class prompts the user for the name of a file they want to scan through,
 * creates a HashMap that hold the words that show up in the scanned text and
 * how many times they each show up in the file, and lastly it creates a new 
 * File to print the HashMap to.
 * 
 * @author Troy Alcaraz
 */

public class ReadPrintFile
{
    
    private String fileName = "";                   //The name of the file wanted to be opened
    private Scanner gen = new Scanner(System.in);   //Scanner that reads in the name of the file being opened
    private Scanner scan;                           //Scanner that reads through the opened file
    private Counter count = new Counter();          //Able to change total and unique word totals
    private File writeFile;                         //The file that will have data written to it
    HashMap <String, Integer> wordList = new HashMap <> (); //Hash map that will hold all the words and # of times they show up in text file
    
    /**
     * Prompts the user for a file name and waits for user input
     * Tries to find the file name that was inputed
     * prints "File found" if the file exists, prints "File not found" otherwise
     */
    
    public void openReadFile()
    {
        System.out.println("Enter file name: ");    //Prompts the user for a file name
        fileName = gen.nextLine();                  //stores the user's input in the variable "fileName"
        
        try
        {
            scan = new Scanner(new File(fileName)); //Tries to scan through a file
            System.out.println("File found!");      //Prints a string if file is found
        }
        
        catch (Exception e)
        {
            System.out.println("File not found");   //Prints a different string otherwise
            
        }
        
    }
    
    /**
     * Tries to scan though a file, makes all the words from scanned file upper
     * case, and adds each word from the scanned file to a HashMap.
     * prints "Nothing to print" otherwise
     */
    
    public void scanReadFile()
    {
        
        try
        {
            while(scan.hasNext())                   //Tries to scan thorugh a file
            {
                String s = scan.next();             //Temperarely stores a word from a file
                
                s = s.toUpperCase();                //Makes the String "s" all upper case letters to help the HashMap ignore case
                
                for(int x = 0; x < 10; x++)         //This loops deletes multiple punctuations from Strings
                {
                    s = punctuationDeleter(s);      //Deletes a punction from a String
                }
                
                //System.out.println(s);            //Prints the stored word, helps with debugging
                
                count.incTotalWords();              //Increases the total words count by 1
                
                if (!wordList.containsKey(s))
                    wordList.put(s,1);              //adds the String "s" to the Hash map and gives it a value of 1 if it is not already in the hashmap
                
                else if (wordList.containsKey(s))   //If the word is already in the hash map
                {
                    int curNum = wordList.get(s);   //Variable for how many times the word shows up
                    wordList.replace(s, curNum, curNum+1);  //Increments the how many times a word showed up by 1
                    //System.out.println(wordList); //Here to help find bugs if any
                }
            }
            
            //System.out.println(wordList);         //Prints out the entire HashMap, here to help find bugs if any
            //System.out.println("Total words: " + count.getTotalWords());    //Prints out the total words
            //System.out.println("Total unique words: " + wordList.size());   //Prints out the total unique words
            
            scan.close();
        }
        
        catch (Exception e)
        {
            System.out.println("Nothing to print"); //Prints a message if there wasn't a file to scan thorugh
        }
    }
    
    /**
     * Tries to close the file that was opened
     * prints "No file to close" if there isn't a file to close
     */
    
    public void closeFile()
    {
        try
        {
            scan.close();                           //Tries to close the file that was opened
            
        }
        
        catch(Exception e)
        {
            System.out.println("No file to close"); //Prints a message if there wasn't a file to close
        }
    }
    
    /**
     * Tries to create a new file that will have data printed to it
     */
    
    public void createPrintFile()                   //Creates a file to wrtie the data onto
    {
        writeFile = new File("Words.txt");
    
        try
        {
            
            if(!writeFile.exists())                 //If the file does not exist, it creates a new file
                
                  //Tries to create a new file variable named "Words.txt"
                
                writeFile.createNewFile();
        }

        catch (Exception e)                         //Prints a message if the file wasn't created
        {
            System.out.println("File not created");
        }
    }
    
    /**
     * Tries to print a hash map to the file created in the
     * createPrintFile() method
     */
    
    public void writeToPrintFile()
    {   
        try                                         //Tries to make a print writer that prints to the file "Words.txt"
        {
            PrintWriter pw = new PrintWriter(writeFile);
            
            HashMap sortedWordList = sortHashMap(wordList);
            
            pw.print(sortedWordList);                   //Tries to print the hash map in "Words.txt"
            pw.println();
            pw.println("Total words: " + count.getTotalWords());    //Tries to print the total word count for read file
            pw.println("Total unique words: " + wordList.size());   //Tries to print the hash map size
            
            //System.out.println(sortedWordList);
            //System.out.println("Total words: " + count.getTotalWords());
            //System.out.println("Total unique words: " + wordList.size());
            //System.out.println(top5Bottom5(sortedWordList));
                        
        }
        
        catch (Exception e)                         //If it is not able to print to the file it prints a message
        {
            System.out.println("No file to write to");
        }
        
        
    }
    
    /**
     * This method makes a LinkedHashMap from a HashMap and sorts them by value
     * @param myHashMap a HashMap
     * @return a LinkedHashMap that is sorted by values from least to greatest
     */
    
    public HashMap sortHashMap(HashMap myHashMap)
    {
        
        LinkedHashMap<String,Integer> result = wordList.entrySet().stream()
                .sorted(Entry.comparingByValue())
                .collect(Collectors.toMap(Entry::getKey, Entry::getValue, (x,y) -> x, LinkedHashMap::new));
        
        Set<Entry<String, Integer>> myResult = result.entrySet();
        
        return result;

    }
    
    /**
     * This method makes a string of the top 5 most and least popular words in a 
     * text document
     * @param myHashMap a HashMap of Strings as keys and Integers as values
     * @return a String of 10 words from most popular to least
     */
    
    public String top5Bottom5(HashMap myHashMap)
    {
        String t5b5 = "The top 5 most & least popular words: \nTop 5: \n";
        
        ArrayList list = new ArrayList(myHashMap.keySet());
        
        for(int j = list.size()-1; j > (list.size()-6); j--)
        {
            t5b5 += list.get(j) + "\n";
        }
        
        t5b5 += "Last 5: \n";
        
        for(int i = 0; i < 5; i++)
        {
            t5b5 += list.get(i) + "\n";
        }
                
        return t5b5;
    }
    
    /**
     * Deletes any punctuation from the beginning and end of a String
     * @param myString a String of any kind
     * @return a subString of myString without any punctuation
     */
    
    public String punctuationDeleter(String myString)
    {
        //Deletes punctuation from the back of Strings
        
        if (myString.endsWith("."))
            myString = myString.substring(0, (myString.length()-1));
        
        else if (myString.endsWith(","))
            myString = myString.substring(0, (myString.length()-1));
        
        else if (myString.endsWith("?"))
            myString = myString.substring(0, (myString.length()-1));
        
        else if (myString.endsWith("!"))
            myString = myString.substring(0, (myString.length()-1));
        
        else if (myString.endsWith(":"))
            myString = myString.substring(0, (myString.length()-1));
        
        else if (myString.endsWith(";"))
            myString = myString.substring(0, (myString.length()-1));
        
        else if (myString.endsWith("-"))
            myString = myString.substring(0, (myString.length()-1));
        
        else if (myString.endsWith(")"))
            myString = myString.substring(0, (myString.length()-1));
        
        else if (myString.endsWith("]"))
            myString = myString.substring(0, (myString.length()-1));
        
        else if (myString.endsWith("}"))
            myString = myString.substring(0, (myString.length()-1));
        
        else if (myString.endsWith("~"))
            myString = myString.substring(0, (myString.length()-1));
        
        else if (myString.endsWith("'"))
            myString = myString.substring(0, (myString.length()-1));
        
        else if (myString.endsWith(">"))
            myString = myString.substring(0, (myString.length()-1));
        
        else if (myString.endsWith("&"))
            myString = myString.substring(0, (myString.length()-1));
        
        else if (myString.endsWith("#"))
            myString = myString.substring(0, (myString.length()-1));
        
        else if (myString.endsWith("*"))
            myString = myString.substring(0, (myString.length()-1));
        
        //Deletes punctuation from the front of Strings
        
        else if (myString.startsWith("."))
            myString = myString.substring(1, myString.length());
        
        else if (myString.startsWith(","))
            myString = myString.substring(1, myString.length());
        
        else if (myString.startsWith("?"))
            myString = myString.substring(1, myString.length());
        
        else if (myString.startsWith("!"))
            myString = myString.substring(1, myString.length());
        
        else if (myString.startsWith(":"))
            myString = myString.substring(1, myString.length());
        
        else if (myString.startsWith(";"))
            myString = myString.substring(1, myString.length());
        
        else if (myString.startsWith("-"))
            myString = myString.substring(1, myString.length());
        
        else if (myString.startsWith("("))
            myString = myString.substring(1, myString.length());
        
        else if (myString.startsWith("["))
            myString = myString.substring(1, myString.length());
        
        else if (myString.startsWith("{"))
            myString = myString.substring(1, myString.length());
        
        else if (myString.startsWith("~"))
            myString = myString.substring(1, myString.length());
        
        else if (myString.startsWith("<"))
            myString = myString.substring(1, myString.length());
        
        else if (myString.startsWith("&"))
            myString = myString.substring(1, myString.length());
        
        else if (myString.startsWith("#"))
            myString = myString.substring(1, myString.length());
        
        else if (myString.startsWith("*"))
            myString = myString.substring(1, myString.length());
        
        else if (myString.startsWith("'"))
            myString = myString.substring(1, myString.length());
        
        return myString;
        
    }
    
}
