/**
 *
 * @author Troy Alcaraz
 */

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        /*This is a simple main that is able to run on the command line by
        typing hamlet.txt when asked to enter a file name.
        */
        
        
        ReadPrintFile controler = new ReadPrintFile();
        
        controler.openReadFile();
        
        controler.createPrintFile();
                
        controler.scanReadFile();
        
        controler.writeToPrintFile();
        
        controler.closeFile();
        
    }
    
}
