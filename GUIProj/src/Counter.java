
/**
 *This class helps manage the total and unique word counts with getters, setters,
 * and with a method that increments the total word count by one.
 * 
 * @author Troy Alcaraz
 */

public class Counter {
    
    private int totalWords;
    private int uniqueWords;

    public int getTotalWords() 
    {
        return totalWords;
    }

    public int getUniqueWords() 
    {
        return uniqueWords;
    }
    
    public int incTotalWords()
    {
        totalWords++;
        return totalWords;
    }

    public void setUniqueWords(int uniqueWords) {
        this.uniqueWords = uniqueWords;
    }
    
}
